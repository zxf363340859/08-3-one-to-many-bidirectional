package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ParentEntityRepositoryTest extends JpaTestBase {
    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Test
    void should_save_relationship_from_child_site() {
        // TODO
        //
        // 请书写测试实现如下的功能：
        //
        // Given parent 和 child 对象。并且它们均已经持久化。
        // When 我们在 child 对象上为其添加 parent 对象引用，并持久化后。
        // Then 当我们重新查询 parent 对象的时候，可以发现在 parent 对象的 children 列表中存在 child 对象。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForParentEntity = new ClosureValue<>();
        flushAndClear(em -> {
            ChildEntity savedChildEntity = childEntityRepository.save(childEntity);
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            closureValueForParentEntity.setValue(savedParentEntity.getId());
        });

        flushAndClear(em -> {
            childEntity.setParentEntity(parentEntity);
            childEntityRepository.save(childEntity);
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValueForParentEntity.getValue());
            assertEquals(1, parentEntityOptional.get().getChildren().size());
        });
        // --end->
    }

    @Test
    void should_save_and_get_parent_and_child_relationship_at_once() {
        // TODO
        //
        // 请书写测试实现如下的功能：
        //
        // Given parent 和 child 对象。并且它们均没有被持久化。
        // When 我们在 parent 对象上为其添加 child 对象引用，并将 parent 对象持久化。
        // Then 当我们重新查询 child 对象的时候，可以发现 child 对象中存在对 parent 对象的引用。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForChildEntity = new ClosureValue<>();

        flushAndClear(em -> {
            parentEntity.addChildren(childEntity);
            childEntity.setParentEntity(parentEntity);
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
        });

        run(em -> {
            Optional<ChildEntity> childEntityOptional = childEntityRepository.findById(2L);
            assertNotNull(childEntityOptional.get().getParentEntity());
//            assertNotNull(childEntityOptional.get().getParentEntity());
        });
        // --end->
    }

    @Test
    void should_remove_child_item() {
        // TODO
        //
        // 请书写如下的测试。
        //
        // Given parent 和 child 对象，并且其已经相互引用并已经持久化。
        // When 当我们从 parent 的 children 列表中移除 child 对象并持久化时。
        // Then 当我们再次查询 parent 的时候，发现 parent 的 children 列表中为空。同时 child 记录本身也
        //   不复存在了。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForParentEntity = new ClosureValue<>();
        flushAndClear(em -> {
            parentEntity.addChildren(childEntity);
            childEntity.setParentEntity(parentEntity);
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            closureValueForParentEntity.setValue(savedParentEntity.getId());
        });

        flushAndClear(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValueForParentEntity.getValue());
            ParentEntity getParentEntity = parentEntityOptional.get();
            getParentEntity.deleteChildren(childEntity);
            parentEntityRepository.save(getParentEntity);
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValueForParentEntity.getValue());
            assertEquals(0, parentEntityOptional.get().getChildren().size());
        });


        // --end->
    }
}
